/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package capanyilvantarto;

import java.util.TimerTask;
import javax.swing.JOptionPane;

/**
 *
 * @author zsenakistvan
 */
public class Kaloriahasznalat extends TimerTask{
    
    private Capa capa;
    
    public Kaloriahasznalat(Capa capa){
        this.capa = capa;
    }
    
    @Override
    public void run(){
        this.capa.energiafogyas();
    }
    
    public Capa getNewCapa(){
        return this.capa;
    }
   
}
